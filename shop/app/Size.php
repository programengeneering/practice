<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    //
    public $timestamps = false;
    protected $table = 'Sizes';

    public function getSize($id){
        $size = $this->Size($id)->get();
        return $size;
    }

    public function scopeSize($query, $id){
        $query->where('id', $id);
    }
}
