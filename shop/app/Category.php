<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    public $timestamps = false;
    protected $table = 'Categories';

    public function getCategories(){
        $value = $this->Categories()->get();
        return $value;
    }

    public function getcategory($id){
        $value = $this->Category($id)->get();
        return $value;
    }

    /**
     * Скопы
     */

    public function scopeCategories($query){
        $query->where('id', '!=', 0);
    }

    public function scopeCategory($query, $id){
        $query->where('id', $id);
    }
}
