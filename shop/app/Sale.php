<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    //
    public $timestamps = false;
    protected $table = 'Sales';

    public function getPurchases($id){
        $history = $this->Purchases($id)->get([
            'products.product',
            'sales.sale_num',
            'sales.qty',
            'sales.sum',
            'sales.created_at',
            'colors.color',
            'colors.hex',
            'sizes.size'
        ]);
        if($history){
            foreach($history as $value){
                $value->qty = (int)$value->qty;
                $value->id = (int)$value->id;
                $value->product = trim($value->product);
                $value->size = trim($value->size);
                $value->color = trim($value->color);
                $value->sum = (double)$value->sum;
                $value->price = (double)$value->sum / (int)$value->qty;
            }
            return $history;
        } else {
            return false;
        }
    }

    public function getTotal($id){
        $total = $this->Total($id)->sum('sum');
        return round($total,2);
    }

    public function getPurchasesByDates($id){
        $purchases = $this->PurchasesByDates($id)->get();
        if($purchases){
            return $purchases;
        } else {
            return false;
        }
    }

    public function countUserBuyes($id){
        $value = $this->UserBuyes($id)->count();
        return $value;
    }

    /**
     * Scope
     */

    public function scopePurchases($query, $id){
        $query->where('sales.user', $id)
            ->join('products', 'sales.product', '=', 'products.id')
            ->join('colors', 'products.color', '=', 'colors.id')
            ->join('sizes', 'products.size', '=', 'sizes.id')
            ->orderBy('sales.created_at');
    }

    public function scopeTotal($query, $id){
        $query->where('user', $id);
    }

    public function scopePurchasesByDates($query, $id){
        $query->where('user', $id)
            ->orderBy('created_at', 'desc');
    }

    public function scopeUserBuyes($query, $id){
        $query->where('user', $id);
    }
}
