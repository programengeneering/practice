<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    public $timestamps = false;
    protected $table = 'Products';

    public function getProductsByCategory($category){
        $products = $this->ProductsByCategory($category)->get([
            'products.id',
            'products.product',
            'products.qty',
            'products.price',
            'products.category',
            'sizes.size',
            'colors.color',
            'colors.hex',
        ]);
        foreach($products as $value){
            $value->qty = (int)$value->qty;
            $value->id = (int)$value->id;
            $value->category = (int)$value->category;
            $value->price = round($value->price, 3);
            $value->product = trim($value->product);
            $value->size = trim($value->size);
            $value->color = trim($value->color);
        }
        return $products;
    }

    public function getProducts(){
        $products = $this->Products()->get([
            'products.id',
            'products.product',
            'products.qty',
            'products.price',
            'products.category',
            'sizes.size',
            'colors.color',
            'colors.hex',
        ]);
        foreach($products as $value){
            $value->qty = (int)$value->qty;
            $value->id = (int)$value->id;
            $value->category = (int)$value->category;
            $value->price = round($value->price, 3);
            $value->product = trim($value->product);
            $value->size = trim($value->size);
            $value->color = trim($value->color);
        }
        return $products;
    }

    public function countProducts(){
        $count = $this->Products()->count();
        return $count;
    }

    public function getProduct($id){
        $product = $this->Product($id)->get([
            'products.id',
            'products.product',
            'products.qty',
            'products.price',
            'products.category',
            'sizes.size',
            'colors.color',
            'colors.hex',
        ]);
        foreach($product as $value){
            $value->qty = (int)$value->qty;
            $value->id = (int)$value->id;
            $value->category = (int)$value->category;
            $value->price = round($value->price, 3);
            $value->product = trim($value->product);
            $value->size = trim($value->size);
            $value->color = trim($value->color);
        }
        return $product;
    }

    /**
     * Скопы
     */
    public function scopeProductsByCategory($query, $category){
        $query->where('category', $category)
            ->join('colors', 'products.color', '=', 'colors.id')
            ->join('sizes', 'products.size', '=', 'sizes.id');
    }

    public function scopeProducts($query){
        $query->where('products.id', '!=', 0)
            ->join('colors', 'products.color', '=', 'colors.id')
            ->join('sizes', 'products.size', '=', 'sizes.id');
    }

    public function scopeProduct($query, $id){
        $query->where('products.id', $id)
            ->join('colors', 'products.color', '=', 'colors.id')
            ->join('sizes', 'products.size', '=', 'sizes.id');
    }
}
