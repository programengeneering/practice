<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    //
    //
    public $timestamps = false;
    protected $table = 'Colors';

    public function getColor($id){
        $color = $this->Color($id)->get();
        return $color;
    }

    public function scopeColor($query, $id){
        $query->where('id', $id);
    }
}
