<?php

namespace App\Http\Controllers;

use App\User;
use Cart;
use App\Category;
use App\Product;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Request;
use Response;
use Validator;
use Redirect;
use Session;

class PaymentController extends Controller
{
    //
    public function __construct(User $user, Guard $auth, Category $category, Product $product){
        $this->user = $user;
        $this->auth = $auth;
        $this->category = $category;
        $this->product = $product;
    }

    public function index(){
        $cart = Cart::content();
        $cart_count = Cart::count();
        $user = $this->user->getUser($this->auth->user()->id);
        $user['0']->balance = (float)$user['0']->balance;
        $user['0']->discount = (float)$user['0']->discount;
        $bues_count = $this->user->countUserBuyes($this->auth->user()->id);
        $categories = $this->category->getCategories();
        $count_products = $this->product->countProducts();
        return view('dashboard.info.default',[
            'user' => $user['0'],
            'buyes' => $bues_count,
            'categories' => $categories,
            'count_products' => $count_products,
            'cart' => $cart,
            'cart_count' => $cart_count
        ]);
    }

    public function fill(){
        $sum = Input::get('fill');
        $this->user->where('id', $this->auth->user()->id)->increment('balance',$sum );
        return Redirect::to('/info')->with('success', 'Баланс успешно пополнен');
    }
}
