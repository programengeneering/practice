<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\User;
use Redirect;
use Hash;
use Auth;
use Validator;

class AuthController extends Controller
{

    //
    public function __construct(User $user){
        $this->user = $user;
    }

    public function index(){
        return view('login.default');
    }


    public function postLogin(){
        $input = [
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'remember' => Input::get('remember')
        ];
        $rules = [
            'email' => 'required|email',
        ];
        $messages = [
            'email.required' => 'Поле "email" обязательно для заполнения',
            'email.email' => 'Это не Email адрес'
        ];
        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('/login')->with('error', $validator->errors()->getMessages());
        }

        if($input['remember']){
            $remember = true;
        } else {
            $remember = false;
        }
        $auth = Auth::attempt(['email' => $input['email'], 'password' => Input::get('password')], $remember);
        if($auth){
            return Redirect::to('/info');
        } else {
            return Redirect::to('/login')->with('error', 'Извините. Вы не верно ввели логин или пароль');
        }
    }

    public function getLogout(){
        Auth::logout();
        return Redirect::to('/');
    }
}
