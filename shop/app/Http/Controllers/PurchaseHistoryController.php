<?php

namespace App\Http\Controllers;

use App\User;
use Cart;
use App\Color;
use App\Size;
use App\Category;
use App\Product;
use App\Sale;
use App\Budget;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use Response;
use Request;
use Redirect;
use View;
use Carbon\Carbon;

class PurchaseHistoryController extends Controller
{
    //
    public function __construct(User $user,
                                Guard $auth,
                                Category $category,
                                Product $product,
                                Color $color,
                                Size $size,
                                Sale $sale,
                                Budget $budget){
        $this->user = $user;
        $this->auth = $auth;
        $this->category = $category;
        $this->product = $product;
        $this->color = $color;
        $this->size = $size;
        $this->sale = $sale;
        $this->budget = $budget;
    }

    public function index(){
        $cart = Cart::content();
        $cart_count = Cart::count();
        $history = $this->sale->getPurchases($this->auth->user()->id);
        $total = $this->sale->getTotal($this->auth->user()->id);
        $user = $this->user->getUser($this->auth->user()->id);
        $user['0']->balance = (float)$user['0']->balance;
        $user['0']->discount = (float)$user['0']->discount;
        $bues_count = $this->sale->countUserBuyes($this->auth->user()->id);
        $categories = $this->category->getCategories();
        $count_products = $this->product->countProducts();
        foreach($history as $value){
            $dt = Carbon::parse($value->created_at);
            $value->created_at = $dt->toDateTimeString();
        }
        return view('dashboard.purchase_history.default',[
            'user' => $user['0'],
            'buyes' => $bues_count,
            'categories' => $categories,
            'count_products' => $count_products,
            'cart' => $cart,
            'cart_count' => $cart_count,
            'history' => $history,
            'total' => $total
        ]);
    }
}
