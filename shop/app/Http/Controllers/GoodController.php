<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Cart;

class GoodController extends Controller
{
    //
    public function __construct(User $user, Guard $auth, Category $category, Product $product){
        $this->user = $user;
        $this->auth = $auth;
        $this->category = $category;
        $this->product = $product;
    }

    public function index($id){
        $cart = Cart::content();
        $cart_count = Cart::count();
        $user = $this->user->getUser($this->auth->user()->id);
        $categories = $this->category->getCategories();

        $count_products = $this->product->countProducts();
        if($id){

            $products = $this->product->getProductsByCategory($id);
            $category = $this->category->getcategory($id);
            $category = (string)$category['0']->category;

        } else {
            $category = "Все категории";
            $products = $this->product->getProducts();
        }
        return view('dashboard.goods.default',[
            'user' => $user['0'],
            'categories' => $categories,
            'count_products' => $count_products,
            'products' => $products,
            'cart' => $cart,
            'cart_count' => $cart_count,
            'category' => $category
        ]);
    }
}
