<?php

namespace App\Http\Controllers;

use App\User;
use Cart;
use App\Sale;
use App\Category;
use App\Product;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Request;


class InfoController extends Controller
{
    //
    public function __construct(User $user, Guard $auth, Category $category, Product $product, Sale $sale){
        $this->user = $user;
        $this->auth = $auth;
        $this->category = $category;
        $this->product = $product;
        $this->sale = $sale;
    }

    private $array_mnth = array(
        '01'	=> 'Январь',
        '02'	=> 'Февраль',
        '03'	=> 'Март',
        '04'	=> 'Апрель',
        '05'    => 'Май',
        '06'	=> 'Июнь',
        '07'	=> 'Июль',
        '08'	=> 'Август',
        '09'	=> 'Сентябрь',
        '10'	=> 'Октябрь',
        '11'	=> 'Ноябрь',
        '12'	=> 'Декабрь'
    );

    public function index(){
        $cart = Cart::content();
        $cart_count = Cart::count();
        $user = $this->user->getUser($this->auth->user()->id);
        $user['0']->balance = (float)$user['0']->balance;
        $user['0']->discount = (float)$user['0']->discount;
        $bues_count = $this->sale->countUserBuyes($this->auth->user()->id);
        $categories = $this->category->getCategories();
        $count_products = $this->product->countProducts();
        $orders = $this->sale->getPurchasesByDates($this->auth->user()->id);
        $y_graph = array();
        $m_graph = array();
        $month = $this->array_mnth[date('m')];

        $year = date('Y');
        if($orders){
            for($i = 0; $i < count($orders); $i++){
                $date = explode('-',$orders[$i]->created_at);
                if($date['0'] == date('Y')){
                    if(isset($y_graph[$this->array_mnth[$date['1']]])){
                        $y_graph[$this->array_mnth[$date['1']]] = $y_graph[$this->array_mnth[$date['1']]] + $orders[$i]->qty;
                    } else {
                        $y_graph[$this->array_mnth[$date['1']]] = $orders[$i]->qty;
                    }
                }
            }
            for($i = 0; $i < count($orders); $i++){
                $date = explode('-',$orders[$i]->created_at);
                (int)$day = date('d', strtotime($orders[$i]->created_at));
                if($date['0'] == date('Y') && $date['1'] == date('m')){
                    if(isset($m_graph[$day])){
                        $m_graph[$day] = (int)$m_graph[$day] + (int)$orders[$i]->qty;
                    } else {
                        $m_graph[$day] = (int)$orders[$i]->qty;
                    }
                }
            }
            //dd(arra($m_graph));

        }

        return view('dashboard.info.default',[
                'user' => $user['0'],
                'buyes' => $bues_count,
                'categories' => $categories,
                'count_products' => $count_products,
                'cart' => $cart,
                'cart_count' => $cart_count,
                'orders' => $orders,
                'y_graph' => array_reverse($y_graph, true),
                'm_graph' => array_reverse($m_graph, true),
                'month' => $month,
                'year' => $year

            ]);
    }
}
