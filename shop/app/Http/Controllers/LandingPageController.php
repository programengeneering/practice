<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;

class LandingPageController extends Controller
{
    //
    //
    public function __construct(User $user, Guard $auth){
        $this->user = $user;
        $this->auth = $auth;
    }

    public function index(){
        return view('landing.default');
    }
}
