<?php

namespace App\Http\Controllers;

use App\User;
use Cart;
use App\Color;
use App\Size;
use App\Category;
use App\Product;
use App\Sale;
use App\Budget;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use Response;
use Request;
use Redirect;
use View;
use Storage;

class CartController extends Controller
{
    //
    public function __construct(User $user,
                                Guard $auth,
                                Category $category,
                                Product $product,
                                Color $color,
                                Size $size,
                                Sale $sale,
                                Budget $budget){
        $this->user = $user;
        $this->auth = $auth;
        $this->category = $category;
        $this->product = $product;
        $this->color = $color;
        $this->size = $size;
        $this->sale = $sale;
        $this->budget = $budget;
    }



    public function index(){
        $cart = Cart::content();
        $cart_count = Cart::count();
        $cart_total = Cart::total();
        $user = $this->user->getUser($this->auth->user()->id);
        $user['0']->balance = (float)$user['0']->balance;
        $user['0']->discount = (float)$user['0']->discount;
        $bues_count = $this->sale->countUserBuyes($this->auth->user()->id);
        $categories = $this->category->getCategories();
        $count_products = $this->product->countProducts();
        return view('dashboard.cart.default',[
            'user' => $user['0'],
            'buyes' => $bues_count,
            'categories' => $categories,
            'count_products' => $count_products,
            'cart' => $cart,
            'cart_count' => $cart_count,
            'cart_total' => $cart_total
        ]);
    }

    /**
     * Store items to cart
     */
    public function store($id){
        $product = $this->product->getProduct($id);
        $discount = (double)$this->auth->user()->discount;
        if(!$product['0']->qty == 0){
            if($this->auth->user()->discount > 0){
                $price = $product['0']->price - ($product['0']->price * $discount / 100);
            } else {
                $price = $product['0']->price;
            }

            Cart::add(
                    $product['0']->id,
                    $product['0']->product, 1,
                    $price,
                    array('size' => $product['0']->size, 'color' => $product['0']->color, 'hex' => $product['0']->hex, 'exists' => $product['0']->qty)
            );
            return redirect()->back()->with('success', 'Товар успешно добавлен в корзину');
        } else {
            return Redirect::to('/info')->with('error', 'Извините. Данного продукта нет в наличии');
        }
    }

    /**
     * Remove items from cart
     */
    public function remove(){
        $rowId = Cart::search(function($key, $value) { return $key->id == Input::get('id'); });
        Cart::remove($rowId->rowId);
        return redirect()->back()->with('success', 'Товар успешно удален из корзины');
    }

    /**
     * Update items in cart
     */
    public function update(){
        if(Request::ajax()){
            $product = $this->product->getProduct(Input::get('id'));
            if($product['0']->qty < Input::get('qty')){
                $rowId = Cart::search(function($key, $value) { return $key->id == Input::get('id'); });
                Cart::update($rowId->rowId, $product['0']->qty);
                return Response::json(['success' => false, 'message' => 'Недостаточно товара на складе', 'max' => $product['0']->qty, 'cart_total' => Cart::total(), 'cart_count' => Cart::count()]);
            }
            $rowId = Cart::search(function($key, $value) { return $key->id == Input::get('id'); });
            Cart::update($rowId->rowId, Input::get('qty'));
            return Response::json(['success' => true, 'message' => 'Корзина успешно обновленна', 'cart_total' => Cart::total(), 'cart_count' => Cart::count()]);
        }
    }

    /**
     * Order items from cart
     */
    public function buy(){
        if(Cart::total() > (double)$this->auth->user()->balance){
            return redirect()->back()->with('error', 'Недостаточно средств на балансе <a href="#" data-toggle="modal" data-target="#blanceModal">Пополнить? </a>');
        }
        $first_p_num = rand(10, 99);
        $second_p_num = rand(1000, 9999);
        $sale_num = $first_p_num.'-'.$second_p_num;
        foreach(Cart::content() as $value){
            $this->sale->insert([
                'product' => $value->id,
                'sale_num' => $sale_num,
                'qty' => $value->qty,
                'sum' => $value->price * $value->qty,
                'user' => $this->auth->user()->id
            ]);
            $this->user->where('id', $this->auth->user()->id)->decrement('balance', $value->price * $value->qty );
        }
        if($this->auth->user()->discount < 7 && Cart::count() > 10){
            $discount = Cart::total() / 10000;
            $this->user->where('id', $this->auth->user()->id)->increment('discount', $discount);
        }
        $html = View::make('checks.sale_check', ['cart' => Cart::content(), 'total' => Cart::total(), 'count' => Cart::count(), 'sale_num' => $sale_num, 'date' => date('Y-m-d H:m:s')])->render();

        $this->budget->increment('budget', (double)Cart::total());
        Cart::destroy();
        Storage::put('/checks/buyes/'.$sale_num.'.html', $html);
        return redirect()->back()->with('success', 'Товар успешно куплен');
    }

}
