<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => 'web'], function () {
    Route::get('/', 'LandingPageController@index');
    Route::get('/login', 'AuthController@index');

    Route::post('/login', 'AuthController@postLogin');
});

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/info', 'InfoController@index');
    Route::get('/logout', 'AuthController@getLogout');


    /**
     * Роуты корзины
     */
    Route::get('/cart/add/{id}', 'CartController@store');
    Route::get('/cart/', 'CartController@index');
    Route::get('/cart/buy', 'CartController@buy');
    Route::get('/purchase/history', 'PurchaseHistoryController@index');
    Route::post('/cart/remove/', 'CartController@remove');
    Route::post('/cart/update/', 'CartController@update');


    /**
     * Роуты продуктов
     */
    Route::get('/goods/{id}', 'GoodController@index');
    Route::get('/view/goods/{id}', 'ViewGoodController@index');

    /**
     * Роуты оплаты
     */
    Route::post('/payment/fill', 'PaymentController@fill');


});
