<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];

    protected $table = 'Users';

    public function getAllUsers(){
        $users = $this->AllUsers()->get();
        return $users;
    }

    public function getUser($id){
        $user = $this->User($id)->get();
        return $user;
    }


    /**
     * Скопы
     */

    public function scopeAllUsers($query){
        $query->where('id', '!=', 0);
    }

    public function scopeUser($query, $id){
        $query->where('id', $id);
    }

}
