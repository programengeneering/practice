<!DOCTYPE html>
<html>
@include('login.html.header')
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#">
      <!--<img src="{{ asset('images/logo-640-640.png') }}" alt="bundle" width="60px" height="60px"> <br>-->
      bundle</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Войдите для продолжения</p>
    @if(Session::has('error'))
      <div class="callout callout-danger">
        <h4>Внимание</h4>

        <p></p>
      </div>
    @endif

    <form action="{{ url('/login') }}" method="post">
      {!! csrf_field() !!}
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Пароль" name="password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember"> Запомнить
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-success btn-block btn-flat">Вход</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="#">Я забыл свой пароль</a><br>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="{{ asset('libs/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('libs/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('libs/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('libs/plugins/validation/validation.js') }}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
      increaseArea: '20%' // optional
    });
  });

</script>
</body>
</html>
