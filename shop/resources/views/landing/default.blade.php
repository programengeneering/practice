<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />
    <link href="{{ asset('landing/default.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('landing/fonts.css') }}" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<div id="header-wrapper">
    <div id="header" class="container">
        <div id="menu">
            <ul>
                <li class="current_page_item"><a href="#" accesskey="1" title="">Главная</a></li>
                <li><a href="#contacts" accesskey="5" title="">Наши контакты</a></li>
                <li><a href="{{ url('/login') }}" accesskey="6" title="">Вход</a></li>
            </ul>
        </div>
        <div id="logo">
            <h1><a href="#">Milana</a></h1>
            <span>Одежда для детей и подростков</span> </div>
    </div>
</div>
<div id="wrapper1">
    <div id="welcome" class="container">
        <div class="title">
            <h2>Добро пожаловать на сайт магазина детской одежды</h2>
            <span class="byline">Для совершения покупок вам необходимо позвонить по номеру указанному ниже</span>
        </div>
    </div>
</div>
<div id="wrapper2">
    <div id="newsletter" class="container">
        <div class="title">
            <h2>Подписаться на обновления</h2>
        </div>
        <div class="content">
            <form method="post" action="#">
                <div class="row half">
                    <div class="6u">
                        <input type="text" class="text" name="name" placeholder="Имя" />
                    </div>
                    <div class="6u">
                        <input type="text" class="text" name="email" placeholder="Email" />
                    </div>
                </div>
                <div class="row">
                    <div class="12u"> <a href="#" class="button submit">Подписаться</a> </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="wrapper4">
    <div id="footer" class="container">
        <div>
            <header class="title">
                <h2>Рассказать о нас</h2>
                </header>
            <ul class="contact">
                <li><a href="#" class="icon icon-twitter"><span>Twitter</span></a></li>
                <li><a href="#" class="icon icon-facebook"><span></span></a></li>
                <li><a href="#" class="icon icon-dribbble"><span>Pinterest</span></a></li>
                <li><a href="#" class="icon icon-tumblr"><span>Google+</span></a></li>
                <li><a href="#" class="icon icon-rss"><span>Pinterest</span></a></li>
            </ul>
        </div>
    </div>
</div>
<div id="contacts">
    <div id="newsletter" class="container">
        <div class="title">
            <h2>Контакты</h2>
        </div>
        <span class="byline">Адрес: <br /> г. Москва ТЦ "Садовод" 14ый километр МКАД <br> +772 4568 856</span>
    </div>
</div>
<div id="copyright">
    <p>Все права зарегистрирвованы. | Разработано студентами Кыргызского Государственного Технического Университета. Кафедры Программное Обеспечение Компьютерных Систем.</p>
</div>
</body>
</html>
