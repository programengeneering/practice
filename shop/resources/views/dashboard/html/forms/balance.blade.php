<form action="{{ url('/payment/fill') }}" method="post">
        {!! csrf_field() !!}
    <div class="modal-body">
        <div class="form-group">
            <label for="title">Сумма поплнения</label>
            <input type="text" name="fill" class="form-control">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left btn-flat" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary btn-flat">Пополнить баланс</button>
    </div>
</form>