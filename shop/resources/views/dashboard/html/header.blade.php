<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/info') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>b</b>l</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Milana</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Свернуть навигацию</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Cart Menu -->
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        <span class="label label-warning cart_count">{{ $cart_count }}</span>
                    </a>
                    @if($cart_count)
                        <ul class="dropdown-menu">
                            <li class="header">В корзине <span class="cart_count">{{ $cart_count }}</span> элементов</li>
                            <li>
                                <ul class="menu">
                                    @foreach($cart as $value)
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-caret-right" aria-hidden="true"></i> {{ $value->name }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="footer"><a href="{{ url('/cart') }}">Открыть корзину</a></li>
                        </ul>
                    @endif
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs">{{ $user->f_name }} {{ $user->l_name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#">
                                <i class="fa fa-cogs" aria-hidden="true"></i></i> Настройки
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/logout') }}">
                                <i class="fa fa-sign-out" aria-hidden="true"></i> Выход
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
            </ul>
        </div>
    </nav>
</header>