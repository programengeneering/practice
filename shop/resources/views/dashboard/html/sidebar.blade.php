    <!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('images/logo-640-640.png') }}" alt="bundle" width="40px" height="40px" class="image-circle">
            </div>
            <div class="pull-left info">
                <p>{{ $user->f_name }} {{ $user->l_name }}</p>
                <a href="#" data-toggle="modal" data-target="#blanceModal"><i class="fa fa-circle text-primary"></i> Баланс: {{ round($user->balance, 3) }}</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Поиск...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">НАВИГАЦИЯ</li>
            <li class="{{ Request::is('*/info') ? 'active' : '' }}">
                <a href="{{ url('/info') }}">
                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                    <span> Информация для вас</span>
                </a>
            </li>
            <li class="treeview {{ Request::is('*/goods/*') ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-tags" aria-hidden="true"></i>
                    <span>Ассортимент</span>
                    <span class="label label-primary pull-right">{{ $count_products }}</span>
                </a>
                <ul class="treeview-menu">
                    @foreach($categories as $value)
                        <li><a href="{{ url('/goods/'.$value->id) }}"><i class="fa fa-circle-o"></i> {{ $value->category }}</a></li>
                    @endforeach
                        <li><a href="{{ url('/goods/0') }}"><i class="fa fa-circle-o"></i> Весь ассортимент</a></li>
                </ul>
            </li>
            @if($cart_count)
            <li class="{{ Request::is('*/cart') ? 'active' : '' }}">
                <a  href="{{ url('/cart') }}">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <span> Корзина</span>
                    <span class="label label-success pull-right cart_count">{{ $cart_count }}</span>
                </a>
            </li>
            @endif
            <li class="{{ Request::is('*/purchase/history') ? 'active' : '' }}">
                <a href="{{ url('/purchase/history') }}">
                    <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
                    <span> История покупок</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-money" aria-hidden="true"></i>
                    <span> История транзакций</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>