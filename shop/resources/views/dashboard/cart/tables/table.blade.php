 <div class="box-header">
        <h3 class="box-title">Корзина</h3>

        <div class="box-tools">
            <a href="{{ url('/cart/buy') }}" class="btn btn-success btn-flat btn-xs">Совершить покупку</a>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <tr>
                <th>Название</th>
                <th>Количество</th>
                <th>Цвет</th>
                <th>Размер</th>
                <th>Стоимость ед. товара(с учетом скидки)</th>
                <th></th>
            </tr>
            @foreach($cart as $value)
                <tr>
                    <td>{{ $value->name }}</td>
                    <td style="position: relative;">
                        <input type="text" name="qty{{ $value->id }}" class="form-control qty" data-id="{{ $value->id }}" value="{{ $value->qty }}">
                        <i class="fa fa-spinner fa-spin" aria-hidden="true" style="position: absolute; right: 15px; top: 17px; display: none;"></i>
                    </td>
                    <td><span class="label label-success" style="background-color: {{ $value->options->hex }};">{{ $value->options->color }}</span></td>
                    <td>{{ $value->options->size }}</td>
                    <td>{{ $value->price }}</td>
                    <td>
                        <form action="{{ url('cart/remove') }}" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="id" value="{{ $value->id }}">
                            <button class="btn btn-danger btn-flat btn-xs">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Общая сумма <span class="cart_total"> {{ $cart_total }} </span></td>
            </tr>
        </table>
    </div><!-- /.box-body -->
