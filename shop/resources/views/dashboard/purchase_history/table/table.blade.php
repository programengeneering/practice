<div class="box-header">
    <h3 class="box-title">История</h3>
    <div class="box-tools">
        <button type="button" class="btn btn-default btn-flat to-excel">Экспорт в Excel</button>
        <button type="button" class="btn btn-default btn-flat" onclick="window.print()">Печать</button>
    </div>
</div>
<div class="box-body">
    <table class="table table-bordered table-hover" id="history_table">
        <thead>
            <tr>
                <th>Номер</th>
                <th>Название</th>
                <th>Количество</th>
                <th>Цвет</th>
                <th>Размер</th>
                <th>Цена</th>
                <th>Сумма</th>
                <th>Дата</th>
                <th ></th>
            </tr>
        </thead>
        <tbody>
        @foreach($history as $value)
            <tr>
                <td>{{ $value->sale_num }}</td>
                <td>{{ $value->product }}</td>
                <td>{{ $value->qty }}</td>
                <td><span class="label label-success" style="background-color: {{ $value->hex }};">{{ $value->color }}</span></td>
                <td>{{ $value->size }}</td>
                <td>{{ $value->price }}</td>
                <td>{{ $value->sum }}</td>
                <td>{{ $value->created_at }}</td>
                <td>
                    <button type="button" class="btn btn-primary btn-flat btn-xs hidden-print">Посмотреть</button>
                    <a href="{{ url('checks/buyes/'.$value->sale_num.'.html') }}" target="_blank" class="btn btn-success btn-flat btn-xs hidden-print">Показать чек</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div><!-- /.box-body -->
