<!DOCTYPE html>
<html>
    @yield('head')
    <link rel="stylesheet" href="{{ asset('libs/plugins/pace/pace.min.css') }}">
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
            });
        });
    </script>
<body class="hold-transition skin-green sidebar-mini">
    <div class="wrapper">
        @include('dashboard.html.header')
        @include('dashboard.html.sidebar')

            @yield('content')

        @include('dashboard.html.footer')
        @include('dashboard.html.modal.balance')
    </div>
    @yield('js')
    <script src="{{ asset('libs/plugins/pace/pace.min.js') }}"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button);
        $(document).ajaxStart(function() { Pace.restart(); });
    </script>

</body>
</html>
