<div class="box-header">
    <h3 class="box-title">{{ $category }}</h3>
    <div class="box-tools">
        <button type="button" class="btn btn-default btn-flat to-excel">Экспорт в Excel</button>
        <button type="button" class="btn btn-default btn-flat" onclick="window.print()">Печать</button>
    </div>
</div>
<div class="box-body">
    <table class="table table-bordered table-hover" id="product_table">
        <thead>
            <tr>
                <th>Продукт</th>
                <th>Количество</th>
                <th>Размер</th>
                <th>Цвет</th>
                <th>Цена</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $value)
                <tr>
                    <td><a href="{{ url('view/goods/'.$value->id) }}">{{ $value->product }}</a></td>
                    <td>{{ $value->qty }}</td>
                    <td>{{ $value->size }}</td>
                    <td><span class="label label-success" style="background-color: {{ $value->hex }};">{{ $value->color }}</span></td>
                    <td>{{ $value->price }} RUB</td>
                    <td>
                        @if($value->qty)
                            <a href="{{ url('cart/add/'.$value->id) }}" class="btn btn-flat btn-success btn-xs">В корзину</a>
                        @else
                            <button type="button" class="btn btn-flat btn-success btn-xs disabled"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i>Нет в наличии</button>
                        @endif
                        <a class="btn btn-flat btn-primary btn-xs" href="{{ url('view/goods/'.$value->id) }}"><i class="fa fa-eye" aria-hidden="true"></i> Просмотреть</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>