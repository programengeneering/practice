<div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Товар</h3>
</div>
<div class="box-body pad">
    <div class="row">
        <div class="col-md-3">
            Изображение
        </div>

        <div class="col-md-6">
            <h2>Название</h2>
            <p><i class="fa fa-text-width"></i> Описание</p>
        </div>
        <div class="col-md-3">
            <h3><span class="label label-info pull-right">3000 RUB</span></h3>
            <br>
            <div class="form-group">
                <label>Цет</label>
                <select aria-hidden="true" tabindex="-1" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
                    <option selected="selected">Alabama</option>
                    <option>Alaska</option>
                    <option>California</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                </select>
            </div>
            <div class="form-group">
                <label>Размер</label>
                <select aria-hidden="true" tabindex="-1" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
                    <option selected="selected">Alabama</option>
                    <option>Alaska</option>
                    <option>California</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                </select>
            </div>



            <button class="btn btn-flat btn-primary pull-right" style="width: 100%;">В корзину</button>
        </div>
    </div>
</div>



