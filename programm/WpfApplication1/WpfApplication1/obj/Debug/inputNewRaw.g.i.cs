﻿#pragma checksum "..\..\inputNewRaw.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "47A5D2F146CF1477A5546792CB5D6BEC"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfApplication1;


namespace WpfApplication1 {
    
    
    /// <summary>
    /// inputNewRaw
    /// </summary>
    public partial class inputNewRaw : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label priority;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label addNewRawLabel;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label rawLabel;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox rawName;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button submitRaw;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label addNewProductLabel;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label productLabel;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox productName;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button submitProd;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label bind;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBox;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBox;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBox1;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\inputNewRaw.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button submitRecipe;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfApplication1;component/inputnewraw.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\inputNewRaw.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.priority = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.image = ((System.Windows.Controls.Image)(target));
            return;
            case 3:
            this.addNewRawLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.rawLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.rawName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.submitRaw = ((System.Windows.Controls.Button)(target));
            return;
            case 7:
            this.addNewProductLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.productLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.productName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.submitProd = ((System.Windows.Controls.Button)(target));
            return;
            case 11:
            this.bind = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.textBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.comboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 14:
            this.comboBox1 = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 15:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.submitRecipe = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

