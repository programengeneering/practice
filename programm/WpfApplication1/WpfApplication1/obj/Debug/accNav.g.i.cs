﻿#pragma checksum "..\..\accNav.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E3B7633982740886289DD4FA65130914"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfApplication1;


namespace WpfApplication1 {
    
    
    /// <summary>
    /// inputData
    /// </summary>
    public partial class inputData : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\accNav.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label priority;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\accNav.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\accNav.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button inputPurchaseOfRaw;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\accNav.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button inputProduction;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\accNav.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button inputProductSale;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\accNav.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button inputNewRaw;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\accNav.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button edit;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\accNav.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button submit_Copy;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfApplication1;component/accnav.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\accNav.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.priority = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.image = ((System.Windows.Controls.Image)(target));
            return;
            case 3:
            this.inputPurchaseOfRaw = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\accNav.xaml"
            this.inputPurchaseOfRaw.Click += new System.Windows.RoutedEventHandler(this.inputPurchaseOfRaw_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.inputProduction = ((System.Windows.Controls.Button)(target));
            
            #line 38 "..\..\accNav.xaml"
            this.inputProduction.Click += new System.Windows.RoutedEventHandler(this.inputProduction_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.inputProductSale = ((System.Windows.Controls.Button)(target));
            
            #line 63 "..\..\accNav.xaml"
            this.inputProductSale.Click += new System.Windows.RoutedEventHandler(this.inputProductSale_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.inputNewRaw = ((System.Windows.Controls.Button)(target));
            
            #line 88 "..\..\accNav.xaml"
            this.inputNewRaw.Click += new System.Windows.RoutedEventHandler(this.inputNewRaw_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.edit = ((System.Windows.Controls.Button)(target));
            
            #line 113 "..\..\accNav.xaml"
            this.edit.Click += new System.Windows.RoutedEventHandler(this.edit_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.submit_Copy = ((System.Windows.Controls.Button)(target));
            
            #line 138 "..\..\accNav.xaml"
            this.submit_Copy.Click += new System.Windows.RoutedEventHandler(this.submit_Copy_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

