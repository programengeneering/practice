﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for Navigation.xaml
    /// </summary>
    public partial class Navigation : Window
    {
        public Navigation()
        {
            InitializeComponent();
        }

        private void inputData_Click(object sender, RoutedEventArgs e)
        {
            inputData input_Data = new inputData();
            input_Data.Show();
            input_Data.Left = this.Left;
            input_Data.Top = this.Top;

        }

        private void simplex_Click(object sender, RoutedEventArgs e)
        {
            planNav simplex = new planNav();
            simplex.Show();
            simplex.Left = this.Left;
            simplex.Top = this.Top;

        }

        private void reports_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Модуль находится в разработке");
        }

        private void prediction_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Модуль находится в разработке");
        }

        private void accountant_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Модуль находится в разработке");
        }

        private void newUser_Click(object sender, RoutedEventArgs e)
        {
            nav_newUser user = new nav_newUser();
            user.Show();
            user.Left = this.Left;
            user.Top = this.Top;
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            MainWindow logout = new MainWindow();
            logout.Show();
            logout.Left = this.Left;
            logout.Top = this.Top;
            this.Close();
        }
    }
}
