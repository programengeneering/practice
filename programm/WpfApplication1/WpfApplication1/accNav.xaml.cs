﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for inputData.xaml
    /// </summary>
    public partial class inputData : Window
    {
        public inputData()
        {
            InitializeComponent();
        }

        private void inputPurchaseOfRaw_Click(object sender, RoutedEventArgs e)
        {
            inputPurchRawForm purchRaw = new inputPurchRawForm();
            purchRaw.Show();
            purchRaw.Left = this.Left;
            purchRaw.Top = this.Top;
            this.Close();
        }

        private void inputProduction_Click(object sender, RoutedEventArgs e)
        {
            inputProduction productionForm = new inputProduction();
            productionForm.Show();
            productionForm.Left = this.Left;
            productionForm.Top = this.Top;
            this.Close();
        }

        private void inputProductSale_Click(object sender, RoutedEventArgs e)
        {
            inputProductSale productSaleForm = new inputProductSale();
            productSaleForm.Show();
            productSaleForm.Left = this.Left;
            productSaleForm.Top = this.Top;
            this.Close();
        }
        private void edit_Click(object sender, RoutedEventArgs e)
        {
            accNav_edit addNewElem = new accNav_edit();
            addNewElem.Show();
            addNewElem.Left = this.Left;
            addNewElem.Top = this.Top;
            this.Close();
        }

        private void inputNewRaw_Click(object sender, RoutedEventArgs e)
        {

            inputNewRaw addNewElem = new inputNewRaw();
            addNewElem.Show();
            addNewElem.Left = this.Left;
            addNewElem.Top = this.Top;
            this.Close();
        }

        private void submit_Copy_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
