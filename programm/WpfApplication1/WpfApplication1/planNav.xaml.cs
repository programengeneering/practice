﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for planNav.xaml
    /// </summary>
    public partial class planNav : Window
    {
        public planNav()
        {
            InitializeComponent();
        }

        private void manual_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Модуль находится в разработке");
        }

        private void auto_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Модуль находится в разработке");
            /*bleicTEST test = new bleicTEST();
            test.Show();
            test.Left = this.Left;
            test.Top = this.Top;
            this.Close();
            */
        }

        private void submit_Copy_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
