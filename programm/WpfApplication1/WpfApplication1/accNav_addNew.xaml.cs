﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for inputNewRaw.xaml
    /// </summary>
    public partial class inputNewRaw : Window
    {
        public inputNewRaw()
        {
            InitializeComponent();
        }

        private void submit_Copy_Click(object sender, RoutedEventArgs e)
        {
            inputData inp = new inputData();
            inp.Show();
            inp.Left = this.Left;
            inp.Top = this.Top;
            this.Close();
        }

        private void submitRaw_Click(object sender, RoutedEventArgs e)//новое сырье добавление
        {
            using (SqlConnection sqlConn = new System.Data.SqlClient.SqlConnection(@"Data Source=DARKMOLTPC;Initial Catalog=practice;User Id=admin;Password=123;"))
            {
                try
                {

                    sqlConn.Open();
                    SqlCommand cmSql = sqlConn.CreateCommand();
                    cmSql.CommandText = "Insert into Materials(material,measure) Values (@parm1,@parm2)";
                    cmSql.Parameters.Add("@parm1", SqlDbType.Char);
                    cmSql.Parameters.Add("@parm2", SqlDbType.TinyInt);
                    cmSql.Parameters["@parm1"].Value = rawName.Text;
                    cmSql.Parameters["@parm2"].Value = idTextBox.Text;
                    //запуск запроса
                    cmSql.ExecuteNonQuery();

                    MessageBox.Show("Отправлено");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неудача");
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            WpfApplication1.practiceDataSet practiceDataSet = ((WpfApplication1.practiceDataSet)(this.FindResource("practiceDataSet")));
            // Загрузить данные в таблицу Measures. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.MeasuresTableAdapter practiceDataSetMeasuresTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.MeasuresTableAdapter();
            practiceDataSetMeasuresTableAdapter.Fill(practiceDataSet.Measures);
            System.Windows.Data.CollectionViewSource measuresViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("measuresViewSource")));
            measuresViewSource.View.MoveCurrentToFirst();
            // Загрузить данные в таблицу Colors. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.ColorsTableAdapter practiceDataSetColorsTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.ColorsTableAdapter();
            practiceDataSetColorsTableAdapter.Fill(practiceDataSet.Colors);
            System.Windows.Data.CollectionViewSource colorsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("colorsViewSource")));
            colorsViewSource.View.MoveCurrentToFirst();
            // Загрузить данные в таблицу Sizes. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.SizesTableAdapter practiceDataSetSizesTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.SizesTableAdapter();
            practiceDataSetSizesTableAdapter.Fill(practiceDataSet.Sizes);
            System.Windows.Data.CollectionViewSource sizesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("sizesViewSource")));
            sizesViewSource.View.MoveCurrentToFirst();
            // Загрузить данные в таблицу Products. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.ProductsTableAdapter practiceDataSetProductsTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.ProductsTableAdapter();
            practiceDataSetProductsTableAdapter.Fill(practiceDataSet.Products);
            System.Windows.Data.CollectionViewSource productsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("productsViewSource")));
            productsViewSource.View.MoveCurrentToFirst();
            // Загрузить данные в таблицу Materials. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.MaterialsTableAdapter practiceDataSetMaterialsTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.MaterialsTableAdapter();
            practiceDataSetMaterialsTableAdapter.Fill(practiceDataSet.Materials);
            System.Windows.Data.CollectionViewSource materialsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("materialsViewSource")));
            materialsViewSource.View.MoveCurrentToFirst();
        }

        private void submitProd_Click(object sender, RoutedEventArgs e)//новый продукт добавление
        {
            using (SqlConnection sqlConn = new System.Data.SqlClient.SqlConnection(@"Data Source=DARKMOLTPC;Initial Catalog=practice;User Id=admin;Password=123;"))
            {
                try
                {

                    sqlConn.Open();
                    SqlCommand cmSql = sqlConn.CreateCommand();
                    cmSql.CommandText = "Insert into Products(product,color,size,measure) Values (@parm1,@parm2,@parm3,@parm4)";
                    cmSql.Parameters.Add("@parm1", SqlDbType.Char);
                    cmSql.Parameters.Add("@parm2", SqlDbType.TinyInt);
                    cmSql.Parameters.Add("@parm3", SqlDbType.TinyInt);
                    cmSql.Parameters.Add("@parm4", SqlDbType.TinyInt);
                    cmSql.Parameters["@parm1"].Value = productName.Text;
                    cmSql.Parameters["@parm2"].Value = idTextBox1.Text;
                    cmSql.Parameters["@parm3"].Value = idTextBox2.Text;
                    cmSql.Parameters["@parm4"].Value = idTextBox3.Text;
                    //запуск запроса
                    cmSql.ExecuteNonQuery();

                    MessageBox.Show("Отправлено");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неудача");
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void submitRecipe_Click(object sender, RoutedEventArgs e)//добавление норм расхода&&&&&&&&почемуто не передает цвет и размер
        {
            using (SqlConnection sqlConn = new System.Data.SqlClient.SqlConnection(@"Data Source=DARKMOLTPC;Initial Catalog=practice;User Id=admin;Password=123;"))
            {
                try
                {

                    sqlConn.Open();
                    SqlCommand cmSql = sqlConn.CreateCommand();
                    cmSql.CommandText = "Insert into Ingredients(product,material,qty,color,size) Values (@parm1,@parm2,@parm3,@parm4,@parm5)";
                    cmSql.Parameters.Add("@parm1", SqlDbType.Int);
                    cmSql.Parameters.Add("@parm2", SqlDbType.Int);
                    cmSql.Parameters.Add("@parm3", SqlDbType.TinyInt);
                    cmSql.Parameters.Add("@parm4", SqlDbType.TinyInt);
                    cmSql.Parameters.Add("@parm5", SqlDbType.TinyInt);
                    cmSql.Parameters["@parm1"].Value = idTextBox4.Text;
                    cmSql.Parameters["@parm2"].Value = idTextBox5.Text;
                    cmSql.Parameters["@parm3"].Value = textBox.Text;
                    cmSql.Parameters["@parm4"].Value = idTextBox1.Text;
                    cmSql.Parameters["@parm5"].Value = idTextBox2.Text;

                    //запуск запроса
                    cmSql.ExecuteNonQuery();

                    MessageBox.Show("Отправлено");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неудача");
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
