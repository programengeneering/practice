﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for inputProduction.xaml
    /// </summary>
    public partial class inputProduction : Window
    {
        public inputProduction()
        {
            InitializeComponent();
            
        }

        private void submit_Copy_Click(object sender, RoutedEventArgs e)
        {
            inputData inp = new inputData();
            inp.Show();
            inp.Left = this.Left;
            inp.Top = this.Top;
            this.Close();
        }

        private void submit_Click(object sender, RoutedEventArgs e)
        {
            using (SqlConnection sqlConn = new System.Data.SqlClient.SqlConnection(@"Data Source=DARKMOLTPC;Initial Catalog=practice;User Id=admin;Password=123;"))
            {
                try
                {

                    sqlConn.Open();
                    SqlCommand cmSql = sqlConn.CreateCommand();

                    cmSql.CommandText = "Insert into Productions(product,qty,color,size) Values (@parm1,@parm2,@parm3,@parm4)";
                    cmSql.Parameters.Add("@parm1", SqlDbType.Int);
                    cmSql.Parameters.Add("@parm2", SqlDbType.SmallInt);
                    cmSql.Parameters.Add("@parm3", SqlDbType.TinyInt);
                    cmSql.Parameters.Add("@parm4", SqlDbType.TinyInt);
                    cmSql.Parameters["@parm1"].Value =idTextBox2.Text;
                    cmSql.Parameters["@parm2"].Value = quantity.Text;
                    cmSql.Parameters["@parm3"].Value = idTextBox.Text;
                    cmSql.Parameters["@parm4"].Value = idTextBox1.Text;

                    cmSql.ExecuteNonQuery();

                    MessageBox.Show("Отправлено");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неудача");
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            WpfApplication1.practiceDataSet practiceDataSet = ((WpfApplication1.practiceDataSet)(this.FindResource("practiceDataSet")));
            // Загрузить данные в таблицу Products. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.ProductsTableAdapter practiceDataSetProductsTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.ProductsTableAdapter();
            practiceDataSetProductsTableAdapter.Fill(practiceDataSet.Products);
            System.Windows.Data.CollectionViewSource productsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("productsViewSource")));
            productsViewSource.View.MoveCurrentToFirst();
            // Загрузить данные в таблицу Measures. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.MeasuresTableAdapter practiceDataSetMeasuresTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.MeasuresTableAdapter();
            practiceDataSetMeasuresTableAdapter.Fill(practiceDataSet.Measures);
            System.Windows.Data.CollectionViewSource measuresViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("measuresViewSource")));
            measuresViewSource.View.MoveCurrentToFirst();
            // Загрузить данные в таблицу Colors. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.ColorsTableAdapter practiceDataSetColorsTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.ColorsTableAdapter();
            practiceDataSetColorsTableAdapter.Fill(practiceDataSet.Colors);
            System.Windows.Data.CollectionViewSource colorsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("colorsViewSource")));
            colorsViewSource.View.MoveCurrentToFirst();
            // Загрузить данные в таблицу Sizes. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.SizesTableAdapter practiceDataSetSizesTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.SizesTableAdapter();
            practiceDataSetSizesTableAdapter.Fill(practiceDataSet.Sizes);
            System.Windows.Data.CollectionViewSource sizesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("sizesViewSource")));
            sizesViewSource.View.MoveCurrentToFirst();
        }
    }
}
