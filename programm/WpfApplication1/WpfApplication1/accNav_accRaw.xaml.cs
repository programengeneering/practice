﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for inputPurchRawForm.xaml
    /// </summary>
    public partial class inputPurchRawForm : Window
    {
        public inputPurchRawForm()
        {
            InitializeComponent();
            
        }

        private void submit_Copy_Click(object sender, RoutedEventArgs e)
        {
            inputData inp = new inputData();
            inp.Show();
            inp.Left = this.Left;
            inp.Top = this.Top;
            this.Close();
        }

        private void submit_Click(object sender, RoutedEventArgs e)
        {
            using (SqlConnection sqlConn = new System.Data.SqlClient.SqlConnection(@"Data Source=DARKMOLTPC;Initial Catalog=practice;User Id=admin;Password=123;"))
            {
                try
                {

                    sqlConn.Open();
                    SqlCommand cmSql = sqlConn.CreateCommand();
                    cmSql.CommandText = "Insert into Purchases(material,qty,sum) Values (@parm3, @parm1,@parm2)";
                    cmSql.Parameters.Add("@parm1", SqlDbType.SmallInt);
                    cmSql.Parameters.Add("@parm2", SqlDbType.Money);
                    cmSql.Parameters.Add("@parm3", SqlDbType.Int);
                    cmSql.Parameters["@parm1"].Value = quantity.Text;
                    cmSql.Parameters["@parm2"].Value = worth.Text;
                    cmSql.Parameters["@parm3"].Value = idTextBox.Text;
                    //запуск запроса
                    cmSql.ExecuteNonQuery();

                    MessageBox.Show("Отправлено");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неудача");
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            WpfApplication1.practiceDataSet practiceDataSet = ((WpfApplication1.practiceDataSet)(this.FindResource("practiceDataSet")));
            // Загрузить данные в таблицу Materials. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.MaterialsTableAdapter practiceDataSetMaterialsTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.MaterialsTableAdapter();
            practiceDataSetMaterialsTableAdapter.Fill(practiceDataSet.Materials);
            System.Windows.Data.CollectionViewSource materialsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("materialsViewSource")));
            materialsViewSource.View.MoveCurrentToFirst();
        }
    }
}
