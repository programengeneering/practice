﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for accNav_edit.xaml
    /// </summary>
    public partial class accNav_edit : Window
    {
        public accNav_edit()
        {
            InitializeComponent();
        }

        private void submit_Copy_Click(object sender, RoutedEventArgs e)
        {
            inputData inp = new inputData();
            inp.Show();
            inp.Left = this.Left;
            inp.Top = this.Top;
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            WpfApplication1.practiceDataSet practiceDataSet = ((WpfApplication1.practiceDataSet)(this.FindResource("practiceDataSet")));
            // Загрузить данные в таблицу Materials. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.MaterialsTableAdapter practiceDataSetMaterialsTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.MaterialsTableAdapter();
            practiceDataSetMaterialsTableAdapter.Fill(practiceDataSet.Materials);
            System.Windows.Data.CollectionViewSource materialsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("materialsViewSource")));
            materialsViewSource.View.MoveCurrentToFirst();
            // Загрузить данные в таблицу Measures. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.MeasuresTableAdapter practiceDataSetMeasuresTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.MeasuresTableAdapter();
            practiceDataSetMeasuresTableAdapter.Fill(practiceDataSet.Measures);
            System.Windows.Data.CollectionViewSource measuresViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("measuresViewSource")));
            measuresViewSource.View.MoveCurrentToFirst();
            // Загрузить данные в таблицу Products. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.ProductsTableAdapter practiceDataSetProductsTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.ProductsTableAdapter();
            practiceDataSetProductsTableAdapter.Fill(practiceDataSet.Products);
            System.Windows.Data.CollectionViewSource productsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("productsViewSource")));
            productsViewSource.View.MoveCurrentToFirst();
            // Загрузить данные в таблицу Colors. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.ColorsTableAdapter practiceDataSetColorsTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.ColorsTableAdapter();
            practiceDataSetColorsTableAdapter.Fill(practiceDataSet.Colors);
            System.Windows.Data.CollectionViewSource colorsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("colorsViewSource")));
            colorsViewSource.View.MoveCurrentToFirst();
            // Загрузить данные в таблицу Sizes. Можно изменить этот код как требуется.
            WpfApplication1.practiceDataSetTableAdapters.SizesTableAdapter practiceDataSetSizesTableAdapter = new WpfApplication1.practiceDataSetTableAdapters.SizesTableAdapter();
            practiceDataSetSizesTableAdapter.Fill(practiceDataSet.Sizes);
            System.Windows.Data.CollectionViewSource sizesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("sizesViewSource")));
            sizesViewSource.View.MoveCurrentToFirst();
        }

        private void submitRaw_Click(object sender, RoutedEventArgs e)//обновить сырье
        {
           

        }

        private void submitRaw_Click_1(object sender, RoutedEventArgs e)
        {
            using (SqlConnection sqlConn = new System.Data.SqlClient.SqlConnection(@"Data Source=DARKMOLTPC;Initial Catalog=practice;User Id=admin;Password=123;"))
            {
                try
                {
                    //пля синтаксис забыл на обновление в sql https://habrahabr.ru/post/123636/ http://www.cyberforum.ru/ado-net/thread118527.html
                    sqlConn.Open();
                    SqlCommand cmSql = sqlConn.CreateCommand();
                    cmSql.CommandText = "Update Materials  Set  material='" + productName_Copy1.Text + "', measure='" + idTextBox.Text + "'  where id='" + idTextBox1.Text + "' ";
                    /*cmSql.Parameters.Add("@parm1", SqlDbType.Char);
                    cmSql.Parameters.Add("@parm2", SqlDbType.TinyInt);
                    cmSql.Parameters.Add("@parm3", SqlDbType.Int);
                    cmSql.Parameters["@parm1"].Value = productName_Copy1.Text;
                    cmSql.Parameters["@parm2"].Value = idTextBox.Text;
                    cmSql.Parameters["@parm3"].Value = idTextBox1.Text;*/
                    //запуск запроса
                    cmSql.ExecuteNonQuery();

                    MessageBox.Show("Отправлено");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неудача");
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void submitProd_Click(object sender, RoutedEventArgs e)
        {
            using (SqlConnection sqlConn = new System.Data.SqlClient.SqlConnection(@"Data Source=DARKMOLTPC;Initial Catalog=practice;User Id=admin;Password=123;"))
            {
                try
                {
                    //пля синтаксис забыл на обновление в sql https://habrahabr.ru/post/123636/ http://www.cyberforum.ru/ado-net/thread118527.html
                    sqlConn.Open();
                    SqlCommand cmSql = sqlConn.CreateCommand();
                    cmSql.CommandText = "Update Products  Set  product='" + productName_Copy2.Text + "', measure='" + idTextBox5.Text + "',  color='" + idTextBox3.Text + "', size='" + idTextBox4.Text + "' where id='" + idTextBox2.Text + "' ";
                    //запуск запроса
                    cmSql.ExecuteNonQuery();

                    MessageBox.Show("Отправлено");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неудача");
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void submitRecipe_Click(object sender, RoutedEventArgs e)
        {
            using (SqlConnection sqlConn = new System.Data.SqlClient.SqlConnection(@"Data Source=DARKMOLTPC;Initial Catalog=practice;User Id=admin;Password=123;"))
            {
                try
                {
                    //  добавить в бд 
                    sqlConn.Open();
                    SqlCommand cmSql = sqlConn.CreateCommand();
                    cmSql.CommandText = "Update Ingredients  Set  product='" +idTextBox2.Text + "',material='" + idTextBox1.Text + "',color='" + idTextBox3.Text + "', size='" + idTextBox4.Text + "',qty='" + textBox.Text+"' where product='" + idTextBox2.Text + "' and material='" + idTextBox1.Text + "' ";
                    //запуск запроса
                    cmSql.ExecuteNonQuery();

                    MessageBox.Show("Отправлено");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неудача");
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
