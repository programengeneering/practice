﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for nav_newUser.xaml
    /// </summary>
    public partial class nav_newUser : Window
    {
        public nav_newUser()
        {
            InitializeComponent();
        }

        private void submit_Copy_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
